import httpx
import requests
from models.links import *
from models.locators import *
from BaseController import LoginPage, RegionPage, RoomChange, CatalogSettings
from pytest_testrail.plugin import pytestrail


def test_change_products(page, browser_context_args):
    settings_page = CatalogSettings(page)
    settings_page.auth()

    return {
        **browser_context_args,
        "ignore_https_errors": True
    }
