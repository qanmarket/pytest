# @pytestrail.case()
def test_login(page, browser_context_args, httpx_mock):
    # httpx_mock.add_response(method="GET")
    """ with httpx.Client() as client:
        response = client.get(home_pages) """
    login_page = LoginPage(page)
    login_page.navigate()
    login_page.login()
    """Проверяем какой пришёл код ответа"""
    r = requests.get(home_pages)
    """Если код ответа не равен 200, тест падает"""
    assert not r.status_code != 200
    print(f"\nСтатус код {r}")
    text = page.inner_text('.nm-user__name')
    assert text == 'АЛЕКСАНДР'
    print(f"\nПользователь {text} после авторизации, определён!")
    return {**browser_context_args, "ignore_https_errors": True}


def test_login_phone(page, browser_context_args, httpx_mock):
    # httpx_mock.add_response(method="GET")
    """ with httpx.Client() as client:
        response = client.get(home_pages) """
    login_page = LoginPage(page)
    login_page.navigate()
    login_page.login_phone()
    applications = page.inner_text('.application__title')
    assert applications == 'Заявка на ипотеку №2690'
    print(f"\n{applications}")

    return {**browser_context_args, "ignore_https_errors": True}


# @pytestrail.case('C711', 'C717')
def test_index_page(page, browser_context_args):
    page.goto(home_pages)
    r = requests.get(home_pages)
    assert 200 == r.status_code
    return {**browser_context_args, "ignore_https_errors": True}


# @pytestrail.case('C712')
def test_login(page, browser_context_args):
    login_page = LoginPage(page)
    login_page.navigate()
    login_page.login()
    return {**browser_context_args, "ignore_https_errors": True}
