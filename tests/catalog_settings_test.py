from BaseController import LoginPage


def test_login_catalog(page, browser_context_args):
    login_page = LoginPage(page)
    login_page.navigate()
    login_page.login()

    return {
        **browser_context_args,
        "ignore_https_errors": True
    }
