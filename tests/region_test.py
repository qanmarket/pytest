from BaseController import LoginPage, RegionPage, RoomChange
from pytest_testrail.plugin import pytestrail


def test_region_change(page, browser_context_args):
    region_page = RegionPage(page)
    region_page.menu()
    region_page.change()

    return {
        **browser_context_args,
        "ignore_https_errors": True
    }