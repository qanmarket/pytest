import requests
from models.links import *
from BaseController import LoginPage, MiniAnketa
from pytest_testrail.plugin import pytestrail


@pytestrail.case('C713', 'C1326')
def test_login_new_authorize(page, browser_context_args):
    login_page = LoginPage(page)
    login_page.navigate()
    login_page.login()
    login_page = MiniAnketa(page)
    r = requests.get(mortgage)
    assert r.status_code == 200

    return {
        **browser_context_args,
        "ignore_https_errors": True
    }


TESTRAIL_TEST_STATUS = {
    "passed": 1,
    "blocked": 2,
    "untested": 3,
    "retest": 4,
    "failed": 5
}