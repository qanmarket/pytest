import unittest
import pytest
import requests

from models.links import *
from models.locators import *
from BaseController import LoginPage, RegionPage, RoomChange
from playwright.sync_api import Page


class CatalogFilter(unittest.TestCase):
    @pytest.fixture(autouse=True)
    def setup(self, page: Page):
        self.page = page

    def test_filter_catalog(self):
        self.page.goto(allrealty)
        # Click [placeholder="Лучший ЖК"]
        self.page.click("[placeholder=\"Лучший ЖК\"]")
        # Fill [placeholder="Лучший ЖК"]
        for filters in filter_search:
            self.page.fill("[placeholder=\"Лучший ЖК\"]", filters)
            # Press Enter
            self.page.press("[placeholder=\"Лучший ЖК\"]", "Enter")
            self.page.click("text=Найти")


class RoomFilter(unittest.TestCase):
    @pytest.fixture(autouse=True)
    def setup(self, page: Page):
        self.page = page

    def test_filter_rooms(self):
        rooms_page = RoomChange(self.page)
        rooms_page.pageGoto()
        text = self.page.inner_text('.main__title')
        assert text == "ЖК «Три Грации»"


if __name__ == '__main__':
    unittest.main()